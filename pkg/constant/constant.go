package constant

import (
	"fmt"

	"gorm.io/gorm"
)

var BaseUrl = "http://13.250.105.254" // API Ingress nya

var (
	UserServiceUrl        = BaseUrl // + ":8001"
	CompanyServiceUrl     = BaseUrl // + ":8002"
	ApplicantServiceUrl   = BaseUrl // + ":8003"
	JobServiceUrl         = BaseUrl // + ":8004"
	ApplicationServiceUrl = BaseUrl // + ":8005"
)

const CONTEXT_JWT_PAYLOAD_KEY = "jwt_payload_key"

/* CONTENT TYPE */
const ContentTypeApplicationJson = "application/json"

var RecordNotFound = gorm.ErrRecordNotFound

type DegreeType int32

const (
	Elementary   DegreeType = 1
	MiddleSchool DegreeType = 2
	HighSchool   DegreeType = 3
	Associate    DegreeType = 4
	Bachelor     DegreeType = 5
	Master       DegreeType = 6
	Doctor       DegreeType = 7
)

func (e DegreeType) String() string {
	switch e {
	case Elementary:
		return "Sekolah Dasar"
	case MiddleSchool:
		return "Sekolah Menengah Pertama"
	case HighSchool:
		return "Sekolah Menengah Atas"
	case Associate:
		return "Diploma"
	case Bachelor:
		return "S1"
	case Master:
		return "S2"
	case Doctor:
		return "S3"
	default:
		return fmt.Sprintf("%d", int(e))
	}
}

type UserRole string
type UserStatus string

var AllRole = map[UserRole]bool{
	ROLE_ADMIN:     true,
	ROLE_APPLICANT: true,
	ROLE_COMPANY:   true,
}

const (
	ROLE_ADMIN     UserRole = "admin"
	ROLE_APPLICANT UserRole = "applicant"
	ROLE_COMPANY   UserRole = "company"
)

const (
	STATUS_ACTIVE    UserStatus = "active"
	STATUS_PENDING   UserStatus = "pending"
	STATUS_SUSPENDED UserStatus = "suspended"
	STATUS_BANNED    UserStatus = "banned"
)

type JobStatus string

const (
	OPEN     JobStatus = "open"
	CLOSED   JobStatus = "closed"
	FINISHED JobStatus = "finished"
)

type JobType string

const (
	FULL_TIME  JobType = "full time"
	PART_TIME  JobType = "part time"
	CONTRACT   JobType = "contract"
	TEMPORER   JobType = "temporer"
	INTERNSHIP JobType = "internship"
)

type Remote string

const (
	REMOTE_YES       JobType = "yes"
	REMOTE_NO        JobType = "no"
	REMOTE_SOMETIMES JobType = "sometimes"
)
