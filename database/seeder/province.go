package seeder

import (
	"appliers/api-job/internal/model/domain"
	"log"

	"gorm.io/gorm"
)

func provinceTableSeeder(conn *gorm.DB) {
	var provinces = []domain.Province{
		{Name: "ACEH"},
		{Name: "SUMATERA UTARA"},
		{Name: "SUMATERA BARAT"},
		{Name: "RIAU"},
		{Name: "JAMBI"},
		{Name: "SUMATERA SELATAN"},
		{Name: "BENGKULU"},
		{Name: "LAMPUNG"},
		{Name: "KEPULAUAN BANGKA BELITUNG"},
		{Name: "KEPULAUAN RIAU"},
		{Name: "DKI JAKARTA"},
		{Name: "JAWA BARAT"},
		{Name: "JAWA TENGAH"},
		{Name: "DI YOGYAKARTA"},
		{Name: "JAWA TIMUR"},
		{Name: "BANTEN"},
		{Name: "BALI"},
		{Name: "NUSA TENGGARA BARAT"},
		{Name: "NUSA TENGGARA TIMUR"},
		{Name: "KALIMANTAN BARAT"},
		{Name: "KALIMANTAN TENGAH"},
		{Name: "KALIMANTAN SELATAN"},
		{Name: "KALIMANTAN TIMUR"},
		{Name: "KALIMANTAN UTARA"},
		{Name: "SULAWESI UTARA"},
		{Name: "SULAWESI TENGAH"},
		{Name: "SULAWESI SELATAN"},
		{Name: "SULAWESI TENGGARA"},
		{Name: "GORONTALO"},
		{Name: "SULAWESI BARAT"},
		{Name: "MALUKU"},
		{Name: "MALUKU UTARA"},
		{Name: "PAPUA"},
		{Name: "PAPUA BARAT"},
	}

	var count int64
	if err := conn.Model(&domain.Province{}).Count(&count).Error; err != nil {
		log.Printf("cannot seed data provinces, with error %v\n", err)
	} else {
		if count > 0 {
			log.Printf("cannot seed data provinces, table not empty")
		} else {
			if err := conn.Create(&provinces).Error; err != nil {
				log.Printf("cannot seed data provinces, with error %v\n", err)
			}
			log.Println("success seed data provinces")
		}
	}
}
