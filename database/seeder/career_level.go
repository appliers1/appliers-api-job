package seeder

import (
	"appliers/api-job/internal/model/domain"
	"log"

	"gorm.io/gorm"
)

func careerLevelTableSeeder(conn *gorm.DB) {

	var careerLevels = []domain.CareerLevel{
		{Name: "CEO/GM/Director/Senior Manager"},
		{Name: "Manager/Assistant Manager"},
		{Name: "Supervisor/Coordinator"},
		{Name: "Staff (non-management & non-supervisor)"},
	}

	var count int64
	if err := conn.Model(&domain.CareerLevel{}).Count(&count).Error; err != nil {
		log.Printf("cannot seed data careerLevels, with error %v\n", err)
	} else {
		if count > 0 {
			log.Printf("cannot seed data careerLevels, table not empty")
		} else {
			if err := conn.Create(&careerLevels).Error; err != nil {
				log.Printf("cannot seed data careerLevels, with error %v\n", err)
			}
			log.Println("success seed data careerLevels")
		}
	}
}
