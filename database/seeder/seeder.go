package seeder

import "gorm.io/gorm"

func Seed(conn *gorm.DB) {
	provinceTableSeeder(conn)
	categoryTableSeeder(conn)
	careerLevelTableSeeder(conn)
}
