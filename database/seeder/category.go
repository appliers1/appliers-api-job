package seeder

import (
	"appliers/api-job/internal/model/domain"
	"log"

	"gorm.io/gorm"
)

func categoryTableSeeder(conn *gorm.DB) {

	var categories = []domain.Category{
		{Name: "Others"},
		{Name: "Accounting/Finance"},
		{Name: "Admin/Human Resource"},
		{Name: "Sales/Marketing"},
		{Name: "Arts/Media/Communication"},
		{Name: "Services"},
		{Name: "Hotel/Restaurant"},
		{Name: "Education/Training"},
		{Name: "Computer/Information Technology"},
		{Name: "Engineering"},
		{Name: "Manufacturing"},
		{Name: "Sciences"},
		{Name: "Healthcare"},
	}

	var count int64
	if err := conn.Model(&domain.Category{}).Count(&count).Error; err != nil {
		log.Printf("cannot seed data categories, with error %v\n", err)
	} else {
		if count > 0 {
			log.Printf("cannot seed data categories, table not empty")
		} else {
			if err := conn.Create(&categories).Error; err != nil {
				log.Printf("cannot seed data categories, with error %v\n", err)
			}
			log.Println("success seed data categories")
		}
	}
}
