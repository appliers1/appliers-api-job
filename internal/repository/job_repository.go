package repository

import (
	. "appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
	"context"
	"strconv"
	"strings"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type JobRepository interface {
	Save(ctx context.Context, job *Job) (*Job, error)
	Update(ctx context.Context, job *Job, jobId uint) (*Job, error)
	Delete(ctx context.Context, jobId uint) error
	FindById(ctx context.Context, jobId uint) (*Job, error)
	FindAll(ctx context.Context, payload *dto.FilterGetRequest[dto.JobFilter], p *dto.Pagination) ([]Job, *dto.PaginationInfo, error)
	CountById(ctx context.Context, jobId uint) (int, error)
	CountByIdAndCompany(ctx context.Context, jobId uint, companyId uint) (int, error)
}

type JobRepositoryImplSql struct {
	DB *gorm.DB
}

func NewJobRepositoryImplSql(db *gorm.DB) JobRepository {
	return &JobRepositoryImplSql{
		DB: db,
	}
}

func (repository *JobRepositoryImplSql) Save(ctx context.Context, job *Job) (*Job, error) {
	err := repository.DB.WithContext(ctx).Save(job).Error
	if err != nil {
		return nil, err
	}

	insertedJob := Job{}
	repository.DB.WithContext(ctx).Preload(clause.Associations).First(&insertedJob, job.ID)
	return &insertedJob, err
}

func (repository *JobRepositoryImplSql) Update(ctx context.Context, job *Job, jobId uint) (*Job, error) {

	err := repository.DB.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		var err error
		err = tx.Omit(clause.Associations).Model(job).Where("id = ?", jobId).Updates(job).Error
		if err != nil {
			return err
		}

		err = tx.Exec("DELETE FROM job_categories WHERE job_id = ?", jobId).Error
		if err != nil {
			return err
		}

		err = tx.Exec("DELETE FROM job_cities WHERE job_id = ?", jobId).Error
		if err != nil {
			return err
		}

		for _, city := range job.JobCities {
			err = tx.Exec("INSERT INTO job_cities(job_id, city_id) VALUES(?, ?)", jobId, city.ID).Error
			if err != nil {
				return err
			}
		}
		for _, category := range job.Categories {
			err = tx.Exec("INSERT INTO job_categories(job_id, category_id) VALUES(?, ?)", jobId, category.ID).Error
			if err != nil {
				return err
			}
		}

		// return nil will commit the whole transaction
		return nil
	})
	updatedJob := Job{}
	repository.DB.WithContext(ctx).Preload(clause.Associations).First(&updatedJob, jobId)
	return &updatedJob, err
}

func (repository *JobRepositoryImplSql) Delete(ctx context.Context, jobId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&Job{}, jobId).Error
	return err
}

func (repository *JobRepositoryImplSql) FindById(ctx context.Context, jobId uint) (*Job, error) {
	var job = Job{}
	err := repository.DB.WithContext(ctx).Preload(clause.Associations).First(&job, jobId).Error
	if err == nil {
		return &job, nil
	} else {
		return nil, err
	}
}

func (repository *JobRepositoryImplSql) FindAll(ctx context.Context, payload *dto.FilterGetRequest[dto.JobFilter], p *dto.Pagination) ([]Job, *dto.PaginationInfo, error) {
	var jobs []Job
	var count int64

	query := repository.DB.WithContext(ctx).Model(&Job{})

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(name) LIKE ? ", search)
	}

	// Filter by CompanyId
	if payload.Filter.CompanyId != 0 {
		query = query.Where("company_id = ? ", payload.Filter.CompanyId)
	}

	// Filter by Educations
	if payload.Filter.Education != "" {
		eduInt, err := strconv.Atoi(payload.Filter.Education)
		if err == nil {
			for i := eduInt; i <= 7; i++ {
				if i == eduInt {
					query = query.Where("degree_type = ? ", strconv.Itoa(i))
				} else {
					query = query.Or("degree_type = ? ", strconv.Itoa(i))
				}
			}
		}
	}

	// Filter by Cities
	if len(payload.Filter.CityId) != 0 {
		query = query.Joins("JOIN job_cities on job_cities.job_id = jobs.id")
		i := 0
		for _, cityId := range payload.Filter.CategoryId {
			if i == 0 {
				query = query.Where("job_cities.city_id = ? ", cityId)
			} else {
				query = query.Or("job_cities.city_id = ? ", cityId)
			}
			i++
		}
	}

	// Filter by Categories
	if len(payload.Filter.CategoryId) != 0 {
		query = query.Joins("JOIN job_categories on job_categories.job_id = jobs.id")
		i := 0
		for _, categoryId := range payload.Filter.CategoryId {
			if i == 0 {
				query = query.Where("job_categories.category_id = ? ", categoryId)
			} else {
				query = query.Or("job_categories.category_id = ? ", categoryId)
			}
			i++
		}
	}

	// Filter by CareerLevelId
	if len(payload.Filter.CareerLevelId) != 0 {
		query = query.Joins("JOIN career_levels on jobs.career_level_id = career_levels.id")
		i := 0
		for _, careerLevelId := range payload.Filter.CategoryId {
			if i == 0 {
				query = query.Where("career_levels.id = ? ", careerLevelId)
			} else {
				query = query.Or("career_levels.id = ? ", careerLevelId)
			}
			i++
		}
	}

	// fmt.Println("Filter", payload.Filter)

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Preload(clause.Associations).Find(&jobs).Error
	// err := query.Limit(limit).Offset(offset).Find(&jobs).Error

	return jobs, dto.CheckInfoPagination(p, count), err
}

func (repository *JobRepositoryImplSql) CountById(ctx context.Context, jobId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&Job{}).Where("id = ?", jobId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *JobRepositoryImplSql) CountByIdAndCompany(ctx context.Context, jobId uint, companyId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&Job{}).Where("id = ? and company_id = ?", jobId, companyId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}
