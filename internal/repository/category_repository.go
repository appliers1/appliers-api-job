package repository

import (
	. "appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
	"context"
	"strings"

	"gorm.io/gorm"
)

type CategoryRepository interface {
	Save(ctx context.Context, category *Category) (*Category, error)
	Update(ctx context.Context, category *Category, categoryId uint) (*Category, error)
	Delete(ctx context.Context, categoryId uint) error
	FindById(ctx context.Context, categoryId uint) (*Category, error)
	FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]Category, *dto.PaginationInfo, error)
	CountById(ctx context.Context, categoryId uint) (int, error)
	CountByName(ctx context.Context, categoryName string) (int, error)
}

type CategoryRepositoryImplSql struct {
	DB *gorm.DB
}

func NewCategoryRepositoryImplSql(db *gorm.DB) CategoryRepository {
	return &CategoryRepositoryImplSql{
		DB: db,
	}
}

func (repository *CategoryRepositoryImplSql) Save(ctx context.Context, category *Category) (*Category, error) {
	err := repository.DB.WithContext(ctx).Save(category).Error
	return category, err
}

func (repository *CategoryRepositoryImplSql) Update(ctx context.Context, category *Category, categoryId uint) (*Category, error) {
	err := repository.DB.WithContext(ctx).Model(category).Where("id = ?", categoryId).Updates(&category).Error
	updatedAddress := Category{}
	repository.DB.WithContext(ctx).First(&updatedAddress, categoryId)
	return &updatedAddress, err
}

func (repository *CategoryRepositoryImplSql) Delete(ctx context.Context, categoryId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&Category{}, categoryId).Error
	return err
}

func (repository *CategoryRepositoryImplSql) FindById(ctx context.Context, categoryId uint) (*Category, error) {
	var category = Category{}
	err := repository.DB.WithContext(ctx).First(&category, categoryId).Error
	if err == nil {
		return &category, nil
	} else {
		return nil, err
	}
}

func (repository *CategoryRepositoryImplSql) FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]Category, *dto.PaginationInfo, error) {
	var categorys []Category
	var count int64

	query := repository.DB.WithContext(ctx).Model(&Category{})

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(name) LIKE ? ", search)
	}

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Find(&categorys).Error

	return categorys, dto.CheckInfoPagination(p, count), err
}

func (repository *CategoryRepositoryImplSql) CountById(ctx context.Context, categoryId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&Category{}).Where("id = ?", categoryId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *CategoryRepositoryImplSql) CountByName(ctx context.Context, categoryName string) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&Category{}).Where("name = ?", categoryName).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}
