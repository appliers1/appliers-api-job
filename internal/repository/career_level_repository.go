package repository

import (
	. "appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
	"context"
	"strings"

	"gorm.io/gorm"
)

type CareerLevelRepository interface {
	Save(ctx context.Context, careerLevel *CareerLevel) (*CareerLevel, error)
	Update(ctx context.Context, careerLevel *CareerLevel, careerLevelId uint) (*CareerLevel, error)
	Delete(ctx context.Context, careerLevelId uint) error
	FindById(ctx context.Context, careerLevelId uint) (*CareerLevel, error)
	FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]CareerLevel, *dto.PaginationInfo, error)
	CountById(ctx context.Context, careerLevelId uint) (int, error)
	CountByName(ctx context.Context, careerLevelName string) (int, error)
}

type CareerLevelRepositoryImplSql struct {
	DB *gorm.DB
}

func NewCareerLevelRepositoryImplSql(db *gorm.DB) CareerLevelRepository {
	return &CareerLevelRepositoryImplSql{
		DB: db,
	}
}

func (repository *CareerLevelRepositoryImplSql) Save(ctx context.Context, careerLevel *CareerLevel) (*CareerLevel, error) {
	err := repository.DB.WithContext(ctx).Save(careerLevel).Error
	return careerLevel, err
}

func (repository *CareerLevelRepositoryImplSql) Update(ctx context.Context, careerLevel *CareerLevel, careerLevelId uint) (*CareerLevel, error) {
	err := repository.DB.WithContext(ctx).Model(careerLevel).Where("id = ?", careerLevelId).Updates(&careerLevel).Error
	updatedAddress := CareerLevel{}
	repository.DB.WithContext(ctx).First(&updatedAddress, careerLevelId)
	return &updatedAddress, err
}

func (repository *CareerLevelRepositoryImplSql) Delete(ctx context.Context, careerLevelId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&CareerLevel{}, careerLevelId).Error
	return err
}

func (repository *CareerLevelRepositoryImplSql) FindById(ctx context.Context, careerLevelId uint) (*CareerLevel, error) {
	var careerLevel = CareerLevel{}
	err := repository.DB.WithContext(ctx).First(&careerLevel, careerLevelId).Error
	if err == nil {
		return &careerLevel, nil
	} else {
		return nil, err
	}
}

func (repository *CareerLevelRepositoryImplSql) FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]CareerLevel, *dto.PaginationInfo, error) {
	var careerLevels []CareerLevel
	var count int64

	query := repository.DB.WithContext(ctx).Model(&CareerLevel{})

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(careerLevel) LIKE ? ", search)
	}

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Find(&careerLevels).Error

	return careerLevels, dto.CheckInfoPagination(p, count), err
}

func (repository *CareerLevelRepositoryImplSql) CountById(ctx context.Context, careerLevelId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&CareerLevel{}).Where("id = ?", careerLevelId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *CareerLevelRepositoryImplSql) CountByName(ctx context.Context, careerLevelName string) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&CareerLevel{}).Where("name = ?", careerLevelName).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}
