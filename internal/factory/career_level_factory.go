package factory

import (
	"appliers/api-job/internal/repository"

	"gorm.io/gorm"
)

type CareerLevelFactory struct {
	CareerLevelRepository repository.CareerLevelRepository
}

func NewCareerLevelFactory(db *gorm.DB) *CareerLevelFactory {
	return &CareerLevelFactory{
		CareerLevelRepository: repository.NewCareerLevelRepositoryImplSql(db),
	}
}
