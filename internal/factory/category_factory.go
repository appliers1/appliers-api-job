package factory

import (
	"appliers/api-job/internal/repository"

	"gorm.io/gorm"
)

type CategoryFactory struct {
	CategoryRepository repository.CategoryRepository
}

func NewCategoryFactory(db *gorm.DB) *CategoryFactory {
	return &CategoryFactory{
		CategoryRepository: repository.NewCategoryRepositoryImplSql(db),
	}
}
