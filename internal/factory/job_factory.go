package factory

import (
	"appliers/api-job/internal/repository"

	"gorm.io/gorm"
)

type JobFactory struct {
	JobRepository repository.JobRepository
}

func NewJobFactory(db *gorm.DB) *JobFactory {
	return &JobFactory{
		JobRepository: repository.NewJobRepositoryImplSql(db),
	}
}
