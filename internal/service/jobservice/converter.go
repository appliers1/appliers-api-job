package jobservice

import (
	. "appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
	"time"
)

func ToJobResponse(job Job, h CompanyHttpRequest) dto.JobResponse {

	var categoryResponses = []dto.CategoryResponse{}
	for _, val := range job.Categories {
		categoryResponses = append(categoryResponses, dto.CategoryResponse{
			ID:   val.ID,
			Name: val.Name,
		})
	}

	var cityResponses = []dto.CityResponse{}
	for _, val := range job.JobCities {
		cityResponses = append(cityResponses, dto.CityResponse{
			ID:         val.ID,
			Name:       val.Name,
			ProvinceID: val.ProvinceID,
		})
	}

	var careerLevelResponse = dto.CareerLevelResponse{
		ID:   job.CareerLevel.ID,
		Name: job.CareerLevel.Name,
	}

	company, err := h.FetchCompany(job.CompanyID)
	if err != nil {
		company = nil
	}

	return dto.JobResponse{
		ID:             job.ID,
		CompanyID:      job.CompanyID,
		Company:        company,
		JobTitle:       job.JobTitle,
		StartDate:      job.StartDate,
		EndDate:        job.EndDate,
		StartSalary:    job.StartSalary,
		EndSalary:      job.EndSalary,
		Status:         job.Status,
		Categories:     categoryResponses,
		JobCities:      cityResponses,
		JobType:        job.JobType,
		DegreeType:     job.DegreeType.String(),
		RemoteWork:     job.RemoteWork,
		JobDescription: job.JobDescription,
		JobBenefit:     job.JobBenefit,
		YearsOfExp:     job.YearsOfExp,
		CareerLevel:    careerLevelResponse,
	}
}

func ToJobResponseWithGo(job Job, h CompanyHttpRequest, ch chan<- *dto.JobResponse) {

	var categoryResponses = []dto.CategoryResponse{}
	for _, val := range job.Categories {
		categoryResponses = append(categoryResponses, dto.CategoryResponse{
			ID:   val.ID,
			Name: val.Name,
		})
	}

	var cityResponses = []dto.CityResponse{}
	for _, val := range job.JobCities {
		cityResponses = append(cityResponses, dto.CityResponse{
			ID:         val.ID,
			Name:       val.Name,
			ProvinceID: val.ProvinceID,
		})
	}

	var careerLevelResponse = dto.CareerLevelResponse{
		ID:   job.CareerLevel.ID,
		Name: job.CareerLevel.Name,
	}

	company, err := h.FetchCompany(job.CompanyID)
	if err != nil {
		company = nil
	}

	jb := &dto.JobResponse{
		ID:             job.ID,
		CompanyID:      job.CompanyID,
		Company:        company,
		JobTitle:       job.JobTitle,
		StartDate:      job.StartDate,
		EndDate:        job.EndDate,
		StartSalary:    job.StartSalary,
		EndSalary:      job.EndSalary,
		Status:         job.Status,
		Categories:     categoryResponses,
		JobCities:      cityResponses,
		JobType:        job.JobType,
		DegreeType:     job.DegreeType.String(),
		RemoteWork:     job.RemoteWork,
		JobDescription: job.JobDescription,
		JobBenefit:     job.JobBenefit,
		YearsOfExp:     job.YearsOfExp,
		CareerLevel:    careerLevelResponse,
	}

	ch <- jb
}

func ToJobDomain(req *dto.JobRequestBody) *Job {
	var dateFormat = "2006-01-02 15:04:05"
	tStart, errS := time.Parse(dateFormat, req.StartDate)

	if errS != nil {
		tStart = time.Now()
	}

	tEnd, errE := time.Parse(dateFormat, req.EndDate)
	if errE != nil {
		tEnd = tStart.AddDate(0, 3, 0)
	}

	job := Job{
		CompanyID:      req.CompanyID,
		JobTitle:       req.JobTitle,
		StartDate:      tStart,
		EndDate:        tEnd,
		StartSalary:    req.StartSalary,
		EndSalary:      req.EndSalary,
		Status:         req.Status,
		Categories:     req.Categories,
		JobCities:      req.JobCities,
		JobType:        req.JobType,
		DegreeType:     req.DegreeType,
		RemoteWork:     req.RemoteWork,
		JobDescription: req.JobDescription,
		JobBenefit:     req.JobBenefit,
		YearsOfExp:     req.YearsOfExp,
		CareerLevelID:  req.CareerLevelID,
	}
	return &job
}

func UpdateToJobDomain(req *dto.UpdateJobRequestBody) *Job {

	var dateFormat = "2006-01-02 15:04:05"
	job := Job{}

	if req.JobTitle != nil {
		job.JobTitle = *req.JobTitle
	}
	if req.StartDate != nil {
		tStart, errS := time.Parse(dateFormat, *req.StartDate)

		if errS == nil {
			job.StartDate = tStart
		}
	}
	if req.EndDate != nil {
		tEnd, errS := time.Parse(dateFormat, *req.EndDate)

		if errS == nil {
			job.EndDate = tEnd
		}
	}
	if req.StartSalary != nil {
		job.StartSalary = *req.StartSalary
	}
	if req.EndSalary != nil {
		job.EndSalary = *req.EndSalary
	}
	if req.Status != nil {
		job.Status = *req.Status
	}
	if req.Categories != nil {
		job.Categories = *req.Categories
	}
	if req.JobCities != nil {
		job.JobCities = *req.JobCities
	}
	if req.JobType != nil {
		job.JobType = *req.JobType
	}
	if req.DegreeType != nil {
		job.DegreeType = *req.DegreeType
	}
	if req.RemoteWork != nil {
		job.RemoteWork = *req.RemoteWork
	}
	if req.JobDescription != nil {
		job.JobDescription = *req.JobDescription
	}
	if req.JobBenefit != nil {
		job.JobBenefit = *req.JobBenefit
	}
	if req.YearsOfExp != nil {
		job.YearsOfExp = *req.YearsOfExp
	}
	if req.CareerLevelID != nil {
		job.CareerLevelID = *req.CareerLevelID
	}

	return &job
}
