package jobservice

import (
	"appliers/api-job/internal/factory"
	_mockRepository "appliers/api-job/internal/mocks/repository"
	"appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
	"appliers/api-job/internal/util/jwtpayload"
	"appliers/api-job/pkg/constant"
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var jobRepository _mockRepository.JobRepository
var ctx context.Context
var jobService JobService
var jobFactory = factory.JobFactory{
	JobRepository: &jobRepository,
}
var jobDomain domain.Job
var err error

func setup() {
	jobService = Newservice(&jobFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	jobDomain = domain.Job{
		CompanyID:   1,
		JobTitle:    "Back End Golang",
		StartDate:   time.Now(),
		EndDate:     time.Now(),
		StartSalary: 9000000,
		EndSalary:   12000000,
		Status:      constant.OPEN,
		JobType:     constant.FULL_TIME,
		DegreeType:  constant.Bachelor,
	}
}

func TestDelete(t *testing.T) {
	setup()
	jobRepository.On("CountByIdAndCompany",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	jobRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	jobRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := jobService.Delete(ctx, uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	jobRepository.On("CountByIdAndCompany",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint")).Return(0, err).Once()
	jobRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	jobRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := jobService.Delete(ctx, uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	jobRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	jobRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&jobDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		job, _ := jobService.FindById(ctx, uint(1))
		assert.Equal(t, job.JobTitle, jobDomain.JobTitle)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	jobRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	jobRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 5 | Invalid FindById", func(t *testing.T) {
		_, err := jobService.FindById(ctx, uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	jobUpdate := domain.Job{
		CompanyID:   1,
		JobTitle:    "Front End Golang",
		StartDate:   time.Now(),
		EndDate:     time.Now(),
		StartSalary: 9000000,
		EndSalary:   12000000,
		Status:      constant.OPEN,
		JobType:     constant.FULL_TIME,
		DegreeType:  constant.Bachelor,
	}
	dto := dto.UpdateJobRequestBody{
		JobTitle: &jobUpdate.JobTitle,
	}
	jobRepository.On("CountByIdAndCompany",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	jobRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	jobRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&jobUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		job, _ := jobService.Update(ctx, &dto, uint(1))
		assert.NotEqual(t, job.JobTitle, jobDomain.JobTitle)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	jobUpdate := domain.Job{
		CompanyID:   1,
		JobTitle:    "Front End Golang",
		StartDate:   time.Now(),
		EndDate:     time.Now(),
		StartSalary: 9000000,
		EndSalary:   12000000,
		Status:      constant.OPEN,
		JobType:     constant.FULL_TIME,
		DegreeType:  constant.Bachelor,
	}
	dto := dto.UpdateJobRequestBody{
		JobTitle: &jobUpdate.JobTitle,
	}
	jobRepository.On("CountByIdAndCompany",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint")).Return(0, err).Once()
	jobRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	jobRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		_, err := jobService.Update(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestCreate(t *testing.T) {
	setup()
	dto := dto.JobRequestBody{
		CompanyID:   jobDomain.CompanyID,
		JobTitle:    jobDomain.JobTitle,
		StartDate:   jobDomain.StartDate.String(),
		EndDate:     jobDomain.EndDate.String(),
		StartSalary: jobDomain.StartSalary,
		EndSalary:   jobDomain.EndSalary,
		DegreeType:  jobDomain.DegreeType,
		JobType:     jobDomain.JobType,
		Status:      jobDomain.Status,
	}
	jobRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(&jobDomain, nil).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {
		job, _ := jobService.Create(ctx, &dto)
		assert.Equal(t, job.JobTitle, jobDomain.JobTitle)
	})
}

func TestCreateFail(t *testing.T) {
	setup()
	dto := dto.JobRequestBody{
		CompanyID:   jobDomain.CompanyID,
		JobTitle:    jobDomain.JobTitle,
		StartDate:   jobDomain.StartDate.String(),
		EndDate:     jobDomain.EndDate.String(),
		StartSalary: jobDomain.StartSalary,
		EndSalary:   jobDomain.EndSalary,
		DegreeType:  jobDomain.DegreeType,
		JobType:     jobDomain.JobType,
		Status:      jobDomain.Status,
	}
	jobRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(nil, err).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

		_, err := jobService.Create(ctx, &dto)
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.Job
	data = append(data, jobDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	jobRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.FilterGetRequest[dto.JobFilter]{
			Pagination: pagination,
			Search:     "Back",
			Filter:     dto.JobFilter{},
		}
		jobs, _ := jobService.Find(ctx, &dto)
		assert.Equal(t, jobs.Datas[0].JobTitle, jobDomain.JobTitle)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	jobRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.FilterGetRequest[dto.JobFilter]{
			Pagination: pagination,
			Search:     "Back",
			Filter:     dto.JobFilter{},
		}
		_, err := jobService.Find(ctx, &dto)
		assert.NotNil(t, err)
	})
}
