package jobservice

import (
	"appliers/api-job/internal/model/entity"
	"appliers/api-job/internal/util/client"
	"appliers/api-job/pkg/constant"
	"net/http"
	"strconv"
	"time"
)

type CompanyHttpRequest interface {
	FetchCompany(userId uint) (*entity.Company, error)
}

type httpRequestToCompany struct {
	BaseUrl string
}

func NewHttpRequestToCompany() CompanyHttpRequest {
	return &httpRequestToCompany{
		BaseUrl: constant.CompanyServiceUrl,
	}
}

func (h *httpRequestToCompany) FetchCompany(companyId uint) (*entity.Company, error) {
	var companyID string = strconv.Itoa(int(companyId))
	httpRequest := client.ClientHttpRequest[entity.Company]{
		Url:         h.BaseUrl + "/companies/" + companyID,
		Method:      http.MethodGet,
		Payload:     nil,
		ContentType: constant.ContentTypeApplicationJson,
		Timeout:     10 * time.Second,
	}

	company, err := httpRequest.StandardRequest()
	return company, err
}
