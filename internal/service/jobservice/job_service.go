package jobservice

import (
	"appliers/api-job/internal/factory"
	"appliers/api-job/internal/model/dto"
	"appliers/api-job/internal/util/jwtpayload"
	"context"

	job "appliers/api-job/internal/repository"
	"appliers/api-job/pkg/constant"
	res "appliers/api-job/pkg/util/response"
)

type JobService interface {
	Create(ctx context.Context, payload *dto.JobRequestBody) (*dto.JobResponse, error)
	Update(ctx context.Context, request *dto.UpdateJobRequestBody, jobId uint) (*dto.JobResponse, error)
	Delete(ctx context.Context, jobId uint) error
	FindById(ctx context.Context, jobId uint) (*dto.JobResponse, error)
	Find(ctx context.Context, payload *dto.FilterGetRequest[dto.JobFilter]) (*dto.SearchGetResponse[dto.JobResponse], error)
}

type service struct {
	JobRepository      job.JobRepository
	CompanyHttpRequest CompanyHttpRequest
}

func Newservice(f *factory.JobFactory) JobService {
	return &service{
		JobRepository:      f.JobRepository,
		CompanyHttpRequest: NewHttpRequestToCompany(),
	}
}

func (s *service) Create(ctx context.Context, payload *dto.JobRequestBody) (*dto.JobResponse, error) {
	data, err := s.JobRepository.Save(ctx, ToJobDomain(payload))
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}
	response := ToJobResponse(*data, s.CompanyHttpRequest)
	return &response, nil
}

func (s *service) Update(ctx context.Context, request *dto.UpdateJobRequestBody, jobId uint) (*dto.JobResponse, error) {
	cc := ctx.Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)

	// validasi apakah job ini milik user's company
	if !cc.IsFromAnotherService && cc.Role != constant.ROLE_ADMIN {
		count1, err1 := s.JobRepository.CountByIdAndCompany(ctx, jobId, uint(cc.InstitutionId))
		if err1 != nil || count1 != 1 {
			res.ErrorBuilder(&res.ErrorConstant.Unauthorized, err1)
		}
	}

	count, err := s.JobRepository.CountById(ctx, jobId)

	if err == nil {
		if count == 1 {

			data, err := s.JobRepository.Update(ctx, UpdateToJobDomain(request), jobId)

			if err == nil {
				data.ID = jobId
				jobResponse := ToJobResponse(*data, s.CompanyHttpRequest)
				return &jobResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Delete(ctx context.Context, jobId uint) error {
	cc := ctx.Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)

	// validasi apakah job ini milik user's company
	if !cc.IsFromAnotherService && cc.Role != constant.ROLE_ADMIN {
		count1, err1 := s.JobRepository.CountByIdAndCompany(ctx, jobId, uint(cc.InstitutionId))
		if err1 != nil || count1 != 1 {
			res.ErrorBuilder(&res.ErrorConstant.Unauthorized, err1)
		}
	}

	count, err := s.JobRepository.CountById(ctx, jobId)
	if err == nil {
		if count == 1 {
			err := s.JobRepository.Delete(ctx, jobId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, jobId uint) (*dto.JobResponse, error) {
	count, err := s.JobRepository.CountById(ctx, jobId)
	if err == nil {
		if count == 1 {
			job, err := s.JobRepository.FindById(ctx, jobId)
			if err == nil {
				jobResponse := ToJobResponse(*job, s.CompanyHttpRequest)
				return &jobResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, payload *dto.FilterGetRequest[dto.JobFilter]) (*dto.SearchGetResponse[dto.JobResponse], error) {

	jobs, info, err := s.JobRepository.FindAll(ctx, payload, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var datas []dto.JobResponse
	ch := make(chan *dto.JobResponse)

	for _, job := range jobs {
		go ToJobResponseWithGo(job, s.CompanyHttpRequest, ch)
	}
	for range jobs {
		datas = append(datas, *<-ch)
	}

	result := new(dto.SearchGetResponse[dto.JobResponse])
	result.Datas = datas
	result.PaginationInfo = *info

	// wg.Wait()
	return result, nil
}
