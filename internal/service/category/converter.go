package category

import (
	. "appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
)

func ToCategoryResponse(category Category) dto.CategoryResponse {
	return dto.CategoryResponse{
		ID:   category.ID,
		Name: category.Name,
	}
}

func ToCategoryDomain(req *dto.CategoryRequestBody) *Category {
	company := Category{
		Name: req.Name,
	}
	return &company
}

func UpdateToCategoryDomain(req *dto.UpdateCategoryRequestBody) *Category {
	category := Category{}
	if req.Name != "" {
		category.Name = req.Name
	}

	return &category
}
