package category

import (
	"appliers/api-job/internal/factory"
	_mockRepository "appliers/api-job/internal/mocks/repository"
	"appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
	"appliers/api-job/internal/util/jwtpayload"
	"appliers/api-job/pkg/constant"
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var categoryRepository _mockRepository.CategoryRepository
var ctx context.Context
var categoryService CategoryService
var categoryFactory = factory.CategoryFactory{
	CategoryRepository: &categoryRepository,
}
var categoryDomain domain.Category
var err error

func setup() {
	categoryService = Newservice(&categoryFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	categoryDomain = domain.Category{
		Name: "IT",
	}
}

func TestDelete(t *testing.T) {
	setup()
	categoryRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	categoryRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := categoryService.Delete(ctx, uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	categoryRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	categoryRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := categoryService.Delete(ctx, uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	categoryRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	categoryRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&categoryDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		category, _ := categoryService.FindById(ctx, uint(1))
		assert.Equal(t, category.Name, categoryDomain.Name)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	categoryRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	categoryRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 5 | Invalid FindById", func(t *testing.T) {
		_, err := categoryService.FindById(ctx, uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	categoryUpdate := domain.Category{
		Name: "Marketing",
	}
	dto := dto.UpdateCategoryRequestBody{
		Name: "Marketing",
	}
	categoryRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	categoryRepository.On("CountByName",
		mock.Anything,
		mock.AnythingOfType("string")).Return(0, nil).Once()
	categoryRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&categoryUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		category, _ := categoryService.Update(ctx, &dto, uint(1))
		assert.NotEqual(t, category.Name, categoryDomain.Name)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	dto := dto.UpdateCategoryRequestBody{
		Name: "Marketing",
	}
	categoryRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	categoryRepository.On("CountByName",
		mock.Anything,
		mock.AnythingOfType("string")).Return(1, err).Once()
	categoryRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		_, err := categoryService.Update(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestCreate(t *testing.T) {
	setup()
	dto := dto.CategoryRequestBody{
		Name: categoryDomain.Name,
	}
	categoryRepository.On("CountByName",
		mock.Anything,
		mock.AnythingOfType("string")).Return(0, nil).Once()
	categoryRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(&categoryDomain, nil).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {
		category, _ := categoryService.Create(ctx, &dto)
		assert.Equal(t, category.Name, categoryDomain.Name)
	})
}

func TestCreateFail(t *testing.T) {
	setup()
	dto := dto.CategoryRequestBody{
		Name: categoryDomain.Name,
	}
	categoryRepository.On("CountByName",
		mock.Anything,
		mock.AnythingOfType("string")).Return(1, err).Once()
	categoryRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Create", func(t *testing.T) {

		_, err := categoryService.Create(ctx, &dto)
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.Category
	data = append(data, categoryDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	categoryRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "I",
		}
		jobs, _ := categoryService.Find(ctx, &dto)
		assert.Equal(t, jobs.Datas[0].Name, categoryDomain.Name)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	categoryRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "I",
		}
		_, err := categoryService.Find(ctx, &dto)
		assert.NotNil(t, err)
	})
}
