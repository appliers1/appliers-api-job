package category

import (
	"appliers/api-job/internal/factory"
	"appliers/api-job/internal/model/dto"
	"context"

	category "appliers/api-job/internal/repository"
	res "appliers/api-job/pkg/util/response"
)

type CategoryService interface {
	Create(ctx context.Context, payload *dto.CategoryRequestBody) (*dto.CategoryResponse, error)
	Update(ctx context.Context, request *dto.UpdateCategoryRequestBody, categoryId uint) (*dto.CategoryResponse, error)
	Delete(ctx context.Context, categoryId uint) error
	FindById(ctx context.Context, categoryId uint) (*dto.CategoryResponse, error)
	Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.CategoryResponse], error)
}

type service struct {
	CategoryRepository category.CategoryRepository
}

func Newservice(f *factory.CategoryFactory) CategoryService {
	return &service{
		CategoryRepository: f.CategoryRepository,
	}
}

func (s *service) Create(ctx context.Context, payload *dto.CategoryRequestBody) (*dto.CategoryResponse, error) {
	countName, err := s.CategoryRepository.CountByName(ctx, payload.Name)

	if err == nil {
		if countName == 0 {
			data, err := s.CategoryRepository.Save(ctx, ToCategoryDomain(payload))
			if err != nil {
				return nil, err
			}
			response := ToCategoryResponse(*data)
			return &response, nil
		} else {
			return nil, res.ErrorBuilder(&res.ErrorConstant.HasBeenUsed, err)
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Update(ctx context.Context, request *dto.UpdateCategoryRequestBody, categoryId uint) (*dto.CategoryResponse, error) {

	count, err := s.CategoryRepository.CountById(ctx, categoryId)
	countName, errName := s.CategoryRepository.CountByName(ctx, request.Name)

	if err == nil && errName == nil {
		if count == 1 && countName == 0 {

			data, err := s.CategoryRepository.Update(ctx, UpdateToCategoryDomain(request), categoryId)

			if err == nil {
				data.ID = categoryId
				categoryResponse := ToCategoryResponse(*data)
				return &categoryResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			} else if countName == 1 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.HasBeenUsed, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Delete(ctx context.Context, categoryId uint) error {
	count, err := s.CategoryRepository.CountById(ctx, categoryId)
	if err == nil {
		if count == 1 {
			err := s.CategoryRepository.Delete(ctx, categoryId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, categoryId uint) (*dto.CategoryResponse, error) {
	count, err := s.CategoryRepository.CountById(ctx, categoryId)
	if err == nil {
		if count == 1 {
			category, err := s.CategoryRepository.FindById(ctx, categoryId)
			if err == nil {
				categoryResponse := ToCategoryResponse(*category)
				return &categoryResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.CategoryResponse], error) {

	categorys, info, err := s.CategoryRepository.FindAll(ctx, payload, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var datas []dto.CategoryResponse

	for _, category := range categorys {
		datas = append(datas, ToCategoryResponse(category))
	}

	result := new(dto.SearchGetResponse[dto.CategoryResponse])
	result.Datas = datas
	result.PaginationInfo = *info

	return result, nil
}
