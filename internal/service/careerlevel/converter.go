package careerlevel

import (
	. "appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
)

func ToCareerLevelResponse(careerLevel CareerLevel) dto.CareerLevelResponse {
	return dto.CareerLevelResponse{
		ID:   careerLevel.ID,
		Name: careerLevel.Name,
	}
}

func ToCareerLevelDomain(req *dto.CareerLevelRequestBody) *CareerLevel {
	careerLevel := CareerLevel{
		Name: req.Name,
	}
	return &careerLevel
}

func UpdateToCareerLevelDomain(req *dto.UpdateCareerLevelRequestBody) *CareerLevel {
	careerLevel := CareerLevel{}

	if req.Name != "" {
		careerLevel.Name = req.Name
	}

	return &careerLevel
}
