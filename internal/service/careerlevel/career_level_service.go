package careerlevel

import (
	"appliers/api-job/internal/factory"
	"appliers/api-job/internal/model/dto"
	"context"

	repository "appliers/api-job/internal/repository"
	res "appliers/api-job/pkg/util/response"
)

type CareerLevelService interface {
	Create(ctx context.Context, payload *dto.CareerLevelRequestBody) (*dto.CareerLevelResponse, error)
	Update(ctx context.Context, request *dto.UpdateCareerLevelRequestBody, careerLevelId uint) (*dto.CareerLevelResponse, error)
	Delete(ctx context.Context, careerLevelId uint) error
	FindById(ctx context.Context, careerLevelId uint) (*dto.CareerLevelResponse, error)
	Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.CareerLevelResponse], error)
}

type service struct {
	CareerLevelRepository repository.CareerLevelRepository
}

func Newservice(f *factory.CareerLevelFactory) CareerLevelService {
	return &service{
		CareerLevelRepository: f.CareerLevelRepository,
	}
}

func (s *service) Create(ctx context.Context, payload *dto.CareerLevelRequestBody) (*dto.CareerLevelResponse, error) {
	countName, err := s.CareerLevelRepository.CountByName(ctx, payload.Name)

	if err == nil {
		if countName == 0 {
			data, err := s.CareerLevelRepository.Save(ctx, ToCareerLevelDomain(payload))
			if err != nil {
				return nil, err
			}
			result := ToCareerLevelResponse(*data)
			return &result, nil
		} else {
			return nil, res.ErrorBuilder(&res.ErrorConstant.HasBeenUsed, err)
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Update(ctx context.Context, request *dto.UpdateCareerLevelRequestBody, careerLevelId uint) (*dto.CareerLevelResponse, error) {

	count, err := s.CareerLevelRepository.CountById(ctx, careerLevelId)
	countName, errName := s.CareerLevelRepository.CountByName(ctx, request.Name)
	if err == nil && errName == nil {
		if count == 1 && countName == 0 {

			data, err := s.CareerLevelRepository.Update(ctx, UpdateToCareerLevelDomain(request), careerLevelId)

			if err == nil {
				data.ID = careerLevelId
				careerLevelResponse := ToCareerLevelResponse(*data)
				return &careerLevelResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			} else if countName == 1 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.HasBeenUsed, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Delete(ctx context.Context, careerLevelId uint) error {
	count, err := s.CareerLevelRepository.CountById(ctx, careerLevelId)
	if err == nil {
		if count == 1 {
			err := s.CareerLevelRepository.Delete(ctx, careerLevelId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, careerLevelId uint) (*dto.CareerLevelResponse, error) {
	count, err := s.CareerLevelRepository.CountById(ctx, careerLevelId)
	if err == nil {
		if count == 1 {
			careerLevel, err := s.CareerLevelRepository.FindById(ctx, careerLevelId)
			if err == nil {
				careerLevelResponse := ToCareerLevelResponse(*careerLevel)
				return &careerLevelResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.CareerLevelResponse], error) {

	careerLevels, info, err := s.CareerLevelRepository.FindAll(ctx, payload, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var datas []dto.CareerLevelResponse

	for _, careerLevel := range careerLevels {
		datas = append(datas, ToCareerLevelResponse(careerLevel))
	}

	result := new(dto.SearchGetResponse[dto.CareerLevelResponse])
	result.Datas = datas
	result.PaginationInfo = *info

	return result, nil
}
