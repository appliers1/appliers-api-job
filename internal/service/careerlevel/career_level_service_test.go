package careerlevel

import (
	"appliers/api-job/internal/factory"
	_mockRepository "appliers/api-job/internal/mocks/repository"
	"appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/dto"
	"appliers/api-job/internal/util/jwtpayload"
	"appliers/api-job/pkg/constant"
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var careerLevelRepository _mockRepository.CareerLevelRepository
var ctx context.Context
var careerLevelService CareerLevelService
var careerLevelFactory = factory.CareerLevelFactory{
	CareerLevelRepository: &careerLevelRepository,
}
var careerLevelDomain domain.CareerLevel
var err error

func setup() {
	careerLevelService = Newservice(&careerLevelFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	careerLevelDomain = domain.CareerLevel{
		Name: "MID",
	}
}

func TestDelete(t *testing.T) {
	setup()
	careerLevelRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	careerLevelRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := careerLevelService.Delete(ctx, uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	careerLevelRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	careerLevelRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := careerLevelService.Delete(ctx, uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	careerLevelRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	careerLevelRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&careerLevelDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		careerLevel, _ := careerLevelService.FindById(ctx, uint(1))
		assert.Equal(t, careerLevel.Name, careerLevelDomain.Name)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	careerLevelRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	careerLevelRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 5 | Invalid FindById", func(t *testing.T) {
		_, err := careerLevelService.FindById(ctx, uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	careerLevelUpdate := domain.CareerLevel{
		Name: "Marketing",
	}
	dto := dto.UpdateCareerLevelRequestBody{
		Name: "Marketing",
	}
	careerLevelRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	careerLevelRepository.On("CountByName",
		mock.Anything,
		mock.AnythingOfType("string")).Return(0, nil).Once()
	careerLevelRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&careerLevelUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		careerLevel, _ := careerLevelService.Update(ctx, &dto, uint(1))
		assert.NotEqual(t, careerLevel.Name, careerLevelDomain.Name)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	dto := dto.UpdateCareerLevelRequestBody{
		Name: "Marketing",
	}
	careerLevelRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	careerLevelRepository.On("CountByName",
		mock.Anything,
		mock.AnythingOfType("string")).Return(1, err).Once()
	careerLevelRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		_, err := careerLevelService.Update(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestCreate(t *testing.T) {
	setup()
	dto := dto.CareerLevelRequestBody{
		Name: careerLevelDomain.Name,
	}
	careerLevelRepository.On("CountByName",
		mock.Anything,
		mock.AnythingOfType("string")).Return(0, nil).Once()
	careerLevelRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(&careerLevelDomain, nil).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {
		careerLevel, _ := careerLevelService.Create(ctx, &dto)
		assert.Equal(t, careerLevel.Name, careerLevelDomain.Name)
	})
}

func TestCreateFail(t *testing.T) {
	setup()
	dto := dto.CareerLevelRequestBody{
		Name: careerLevelDomain.Name,
	}
	careerLevelRepository.On("CountByName",
		mock.Anything,
		mock.AnythingOfType("string")).Return(1, err).Once()
	careerLevelRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Create", func(t *testing.T) {

		_, err := careerLevelService.Create(ctx, &dto)
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.CareerLevel
	data = append(data, careerLevelDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	careerLevelRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "I",
		}
		jobs, _ := careerLevelService.Find(ctx, &dto)
		assert.Equal(t, jobs.Datas[0].Name, careerLevelDomain.Name)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	careerLevelRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "I",
		}
		_, err := careerLevelService.Find(ctx, &dto)
		assert.NotNil(t, err)
	})
}
