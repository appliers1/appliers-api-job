package router

import (
	"appliers/api-job/internal/http/controller"
	"appliers/api-job/internal/http/middleware"
	. "appliers/api-job/pkg/constant"

	"github.com/labstack/echo/v4"
)

func JobRouter(e *echo.Echo, c *controller.JobController) {
	e.GET("/jobs", c.Find, middleware.NewAllowedRole(AllRole).Authentication)
	e.POST("/jobs", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.GET("/jobs/:id", c.FindById, middleware.NewAllowedRole(AllRole).Authentication)
	e.PATCH("/jobs/:id", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.DELETE("/jobs/:id", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
}
