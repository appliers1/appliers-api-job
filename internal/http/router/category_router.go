package router

import (
	"appliers/api-job/internal/http/controller"
	"appliers/api-job/internal/http/middleware"
	. "appliers/api-job/pkg/constant"

	"github.com/labstack/echo/v4"
)

func CategoryRouter(e *echo.Echo, c *controller.CategoryController) {
	e.GET("/categories", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.POST("/categories", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.GET("/categories/:id", c.FindById, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.PATCH("/categories/:id", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.DELETE("/categories/:id", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
}
