package router

import (
	"appliers/api-job/internal/http/controller"
	"appliers/api-job/internal/http/middleware"
	. "appliers/api-job/pkg/constant"

	"github.com/labstack/echo/v4"
)

func CareerLevelRouter(e *echo.Echo, c *controller.CareerLevelController) {
	e.GET("/career_levels", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.POST("/career_levels", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.GET("/career_levels/:id", c.FindById, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.PATCH("/career_levels/:id", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.DELETE("/career_levels/:id", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
}
