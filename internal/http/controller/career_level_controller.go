package controller

import (
	"appliers/api-job/internal/factory"
	"appliers/api-job/internal/model/dto"
	service "appliers/api-job/internal/service/careerlevel"
	res "appliers/api-job/pkg/util/response"
	"strconv"

	"github.com/labstack/echo/v4"
)

type CareerLevelController struct {
	CareerLevelService service.CareerLevelService
}

func NewCareerLevelController(f *factory.CareerLevelFactory) *CareerLevelController {
	return &CareerLevelController{
		CareerLevelService: service.Newservice(f),
	}
}

func (controller *CareerLevelController) Create(c echo.Context) error {
	payload := new(dto.CareerLevelRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.CareerLevelService.Create(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CareerLevelController) Update(c echo.Context) error {
	payload := new(dto.UpdateCareerLevelRequestBody)
	err := c.Bind(payload)
	id, errId := strconv.ParseUint(c.Param("address_id"), 10, 32)
	if err != nil || errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.CareerLevelService.Update(c.Request().Context(), payload, uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CareerLevelController) Delete(c echo.Context) error {
	var id, errId = strconv.ParseUint(c.Param("address_id"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, errId).Send(c)
	}

	err := controller.CareerLevelService.Delete(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *CareerLevelController) FindById(c echo.Context) error {
	var id, err = strconv.ParseUint(c.Param("address_id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	user, err := controller.CareerLevelService.FindById(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CareerLevelController) Find(c echo.Context) error {
	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	result, err := controller.CareerLevelService.Find(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.CustomSuccessBuilder(200, result.Datas, "Get company addresses success", nil).Send(c)
}
