package controller

import (
	"appliers/api-job/internal/factory"
	"appliers/api-job/internal/model/dto"
	service "appliers/api-job/internal/service/jobservice"
	"strconv"

	res "appliers/api-job/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type JobController struct {
	JobService service.JobService
}

func NewJobController(f *factory.JobFactory) *JobController {
	return &JobController{
		JobService: service.Newservice(f),
	}
}

func (controller *JobController) Create(c echo.Context) error {
	payload := new(dto.JobRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.JobService.Create(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *JobController) Update(c echo.Context) error {
	payload := new(dto.UpdateJobRequestBody)
	err := c.Bind(payload)
	id, errId := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil || errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.JobService.Update(c.Request().Context(), payload, uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *JobController) Delete(c echo.Context) error {
	var id, errId = strconv.ParseUint(c.Param("id"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, errId).Send(c)
	}

	err := controller.JobService.Delete(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *JobController) FindById(c echo.Context) error {
	var id, err = strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	user, err := controller.JobService.FindById(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *JobController) Find(c echo.Context) error {
	payload := new(dto.FilterGetRequest[dto.JobFilter])
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	result, err := controller.JobService.Find(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.CustomSuccessBuilder(200, result.Datas, "Get company addresses success", nil).Send(c)
}
