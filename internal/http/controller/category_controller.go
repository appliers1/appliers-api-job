package controller

import (
	"appliers/api-job/internal/factory"
	"appliers/api-job/internal/model/dto"
	service "appliers/api-job/internal/service/category"
	"fmt"
	"strconv"

	"appliers/api-job/internal/util/jwtpayload"
	"appliers/api-job/pkg/constant"
	res "appliers/api-job/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type CategoryController struct {
	CategoryService service.CategoryService
}

func NewCategoryController(f *factory.CategoryFactory) *CategoryController {
	return &CategoryController{
		CategoryService: service.Newservice(f),
	}
}

func (controller *CategoryController) Create(c echo.Context) error {

	payload := new(dto.CategoryRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.CategoryService.Create(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CategoryController) Update(c echo.Context) error {
	payload := new(dto.UpdateCategoryRequestBody)
	err := c.Bind(payload)
	id, errId := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil || errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.CategoryService.Update(c.Request().Context(), payload, uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CategoryController) Delete(c echo.Context) error {
	var id, errId = strconv.ParseUint(c.Param("id"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, errId).Send(c)
	}

	err := controller.CategoryService.Delete(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *CategoryController) FindById(c echo.Context) error {

	// test get jwt payload
	cc := c.Request().Context().Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)
	fmt.Println(cc)

	var id, err = strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	user, err := controller.CategoryService.FindById(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CategoryController) Find(c echo.Context) error {
	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	result, err := controller.CategoryService.Find(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.CustomSuccessBuilder(200, result.Datas, "Get company addresses success", nil).Send(c)
}
