package middleware

import (
	"appliers/api-job/internal/util/jwtpayload"
	"appliers/api-job/pkg/constant"
	res "appliers/api-job/pkg/util/response"
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

type AllowedRole struct {
	Roles map[constant.UserRole]bool
}

func NewAllowedRole(roles map[constant.UserRole]bool) *AllowedRole {
	return &AllowedRole{
		Roles: roles,
	}
}

func (a *AllowedRole) Authentication(next echo.HandlerFunc) echo.HandlerFunc {
	var (
		jwtKey = os.Getenv("JWT_KEY")
	)

	return func(c echo.Context) error {
		authToken := c.Request().Header.Get("Authorization")
		if authToken == "" {
			return res.ErrorBuilder(&res.ErrorConstant.Unauthorized, nil).Send(c)
		}

		if authToken == os.Getenv("RANDOM_KEY") {
			payload := jwtpayload.NewJWTPayload(nil, &authToken)
			if payload == nil {
				return res.ErrorBuilder(&res.ErrorConstant.Unauthorized, nil).Send(c)
			}
			newRequest := c.Request().WithContext(context.WithValue(c.Request().Context(), constant.CONTEXT_JWT_PAYLOAD_KEY, payload))
			c.SetRequest(newRequest)
			return next(c)
		}

		splitToken := strings.Split(authToken, "Bearer ")
		token, err := jwt.Parse(splitToken[1], func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected signing method :%v", token.Header["alg"])
			}

			return []byte(jwtKey), nil
		})

		if !token.Valid || err != nil {
			return res.ErrorBuilder(&res.ErrorConstant.Unauthorized, err).Send(c)
		}

		role := constant.UserRole(token.Claims.(jwt.MapClaims)["role"].(string))
		if _, ok := a.Roles[role]; ok {

			// add JWT Payload into context
			payload := jwtpayload.NewJWTPayload(token, nil)
			if payload == nil {
				return res.ErrorBuilder(&res.ErrorConstant.Unauthorized, nil).Send(c)
			}
			newRequest := c.Request().WithContext(context.WithValue(c.Request().Context(), constant.CONTEXT_JWT_PAYLOAD_KEY, payload))
			c.SetRequest(newRequest)

			return next(c)
		} else {
			return res.ErrorBuilder(&res.ErrorConstant.Unauthorized, err).Send(c)
		}
	}
}
