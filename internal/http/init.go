package http

import (
	"appliers/api-job/internal/factory"
	c "appliers/api-job/internal/http/controller"
	"appliers/api-job/internal/http/router"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func Init(e *echo.Echo, db *gorm.DB) {
	router.CategoryRouter(e, c.NewCategoryController(factory.NewCategoryFactory(db)))
	router.CareerLevelRouter(e, c.NewCareerLevelController(factory.NewCareerLevelFactory(db)))
	router.JobRouter(e, c.NewJobController(factory.NewJobFactory(db)))
}
