package entity

type Company struct {
	ID               uint             `json:"id"`
	Name             string           `json:"name"`
	Service          string           `json:"service"`
	About            string           `json:"about"`
	Logo             string           `json:"logo"`
	TotalEmployees   int              `json:"total_employees"`
	Website          string           `json:"website"`
	Email            string           `json:"email"`
	CompanyAddresses []CompanyAddress `json:"company_addresses"`
	// Recruiters       []Recruiter      `json:"recruiters"`
}

type CompanyAddress struct {
	ID         uint   `json:"id"`
	CompanyID  uint   `json:"company_id"`
	Address    string `json:"address"`
	PostalCode string `json:"postal_code"`
	Telp       string `json:"telp"`
}

type CompanyRequest struct {
	Name           string `json:"name"`
	Service        string `json:"service"`
	TotalEmployees int    `json:"total_employees"`
}
