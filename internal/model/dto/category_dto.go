package dto

/* REQUEST */
type CategoryRequestBody struct {
	Name string `json:"name"`
}

type UpdateCategoryRequestBody struct {
	Name string `json:"name"`
}

/* RESPONSE */
type CategoryResponse struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}
