package dto

/* REQUEST */
type CityRequestBody struct {
	Name       string `json:"name"`
	ProvinceID uint   `json:"province_id"`
}

type UpdateCityRequestBody struct {
	Name       *string `json:"name"`
	ProvinceID *uint   `json:"province_id"`
}

/* RESPONSE */
type CityResponse struct {
	ID         uint   `json:"id"`
	Name       string `json:"name"`
	ProvinceID uint   `json:"province_id"`
	// Province   ProvinceResponse `json:"province"`
}
