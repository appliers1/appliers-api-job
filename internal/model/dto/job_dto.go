package dto

import (
	"appliers/api-job/internal/model/domain"
	"appliers/api-job/internal/model/entity"
	"appliers/api-job/pkg/constant"
	"time"
)

/* REQUEST */
type JobRequestBody struct {
	CompanyID      uint                `json:"company_id" validate:"required"`
	JobTitle       string              `json:"job_title" validate:"required"`
	StartDate      string              `json:"start_date" validate:"required"`
	EndDate        string              `json:"end_date"`
	StartSalary    uint                `json:"start_salary"`
	EndSalary      uint                `json:"end_salary"`
	Status         constant.JobStatus  `json:"job_status"`
	Categories     []domain.Category   `json:"categories"`
	JobCities      []domain.City       `json:"job_cities"`
	JobType        constant.JobType    `json:"job_type"`
	DegreeType     constant.DegreeType `json:"degree_type"`
	RemoteWork     constant.Remote     `json:"remote_work"`
	JobDescription string              `json:"job_description"`
	JobBenefit     string              `json:"job_benefit" validate:"required"`
	YearsOfExp     uint                `json:"years_of_exp"`
	CareerLevelID  uint                `json:"career_level_id"`
}

type UpdateJobRequestBody struct {
	JobTitle       *string              `json:"job_title"`
	StartDate      *string              `json:"start_date"`
	EndDate        *string              `json:"end_date"`
	StartSalary    *uint                `json:"start_salary"`
	EndSalary      *uint                `json:"end_salary"`
	Status         *constant.JobStatus  `json:"job_status"`
	Categories     *[]domain.Category   `json:"categories"`
	JobCities      *[]domain.City       `json:"job_cities"`
	JobType        *constant.JobType    `json:"job_type"`
	DegreeType     *constant.DegreeType `json:"degree_type"`
	RemoteWork     *constant.Remote     `json:"remote_work"`
	JobDescription *string              `json:"job_description"`
	JobBenefit     *string              `json:"job_benefit" validate:"required"`
	YearsOfExp     *uint                `json:"years_of_exp"`
	CareerLevelID  *uint                `json:"career_level_id"`
}

/* RESPONSE */
type JobResponse struct {
	ID             uint                `json:"id"`
	CompanyID      uint                `json:"company_id"`
	Company        *entity.Company     `json:"company"`
	JobTitle       string              `json:"job_title"`
	StartDate      time.Time           `json:"start_date"`
	EndDate        time.Time           `json:"end_date"`
	StartSalary    uint                `json:"start_salary"`
	EndSalary      uint                `json:"end_salary"`
	Status         constant.JobStatus  `json:"job_status"`
	Categories     []CategoryResponse  `json:"categories"`
	JobCities      []CityResponse      `json:"job_cities"`
	JobType        constant.JobType    `json:"job_type"`
	DegreeType     string              `json:"degree_type"`
	RemoteWork     constant.Remote     `json:"remote_work"`
	JobDescription string              `json:"job_description"`
	JobBenefit     string              `json:"job_benefit"`
	YearsOfExp     uint                `json:"years_of_exp"`
	CareerLevel    CareerLevelResponse `json:"career_level"`
}

type JobFilter struct {
	CompanyId     uint   `json:"company_id"`
	CategoryId    []uint `json:"category_ids"`
	CareerLevelId []uint `json:"career_level_ids"`
	CityId        []uint `json:"city_ids"`
	Education     string `json:"min_degree_type"`
}
