package dto

type ProvinceRequestBody struct {
	Name string `json:"name"`
}

type UpdateProvinceRequestBody struct {
	Name string `json:"name"`
}

/* RESPONSE */
type ProvinceResponse struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}
