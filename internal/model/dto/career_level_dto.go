package dto

/* REQUEST */
type CareerLevelRequestBody struct {
	Name string `json:"name"`
}

type UpdateCareerLevelRequestBody struct {
	Name string `json:"name"`
}

/* RESPONSE */
type CareerLevelResponse struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}
