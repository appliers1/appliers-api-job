package domain

import (
	"appliers/api-job/pkg/constant"
	"time"

	"gorm.io/gorm"
)

type Job struct {
	gorm.Model
	CompanyID      uint                `json:"company_id"`
	JobTitle       string              `json:"job_title" gorm:"size:200;not null;index:,class:FULLTEXT"`
	StartDate      time.Time           `json:"start_date" gorm:"not null"`
	EndDate        time.Time           `json:"end_date" gorm:""`
	StartSalary    uint                `json:"start_salary"`
	EndSalary      uint                `json:"end_salary"`
	Status         constant.JobStatus  `json:"job_status" sql:"type:jobStatus" gorm:"type:enum('open','closed','finished');not null;default:'open'"`
	Categories     []Category          `json:"categories" gorm:"many2many:job_categories"`
	JobCities      []City              `json:"job_cities" gorm:"many2many:job_cities;not null"`
	JobType        constant.JobType    `json:"job_type" sql:"type:jobType" gorm:"type:enum('full time','part time','contract','temporer','internship')"`
	DegreeType     constant.DegreeType `json:"degree_type" sql:"type:degree" gorm:"type:enum('1','2','3','4','5','6','7')"`
	RemoteWork     constant.Remote     `json:"remote_work" sql:"type:remote" gorm:"type:enum('yes','no','sometimes');default:sometimes"`
	JobDescription string              `json:"job_description; not null;"`
	JobBenefit     string              `json:"job_benefit"`
	YearsOfExp     uint                `json:"years_of_exp" gorm:"size:3"`
	CareerLevelID  uint                `json:"career_level_id"`
	CareerLevel    CareerLevel         `json:"career_level"`
}

type City struct {
	ID         uint   `json:"id" gorm:"primarykey;autoIncrement;"`
	Name       string `json:"name" gorm:"size:100;not null"`
	ProvinceID uint   `json:"province_id"`
	// Province   Province       `json:"province"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type Province struct {
	ID   uint   `json:"id" gorm:"primarykey;autoIncrement;"`
	Name string `json:"name" gorm:"size:100;not null"`
	// Cities    []City         `json:"cities"`
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
