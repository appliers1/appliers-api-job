package domain

import "gorm.io/gorm"

type CareerLevel struct {
	gorm.Model
	Name string `json:"name" gorm:"size:100;not null"`
}
